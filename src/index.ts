// import cluster from "cluster";
// import os from "os";
import app from "./server";
import * as dotenv from "dotenv";
dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

// comments out because using pm2 to run cluster mode
// const totalCPUs = os.cpus().length;

// if (cluster.isPrimary) {
//   console.log(`Total number of CPUs is ${totalCPUs}`);
//   for (let i = 0; i < totalCPUs; i += 1) {
//     cluster.fork();
//   }
//   cluster.on("online", (worker) => {
//     console.log(`Worker Id is ${worker.id} and PID is ${worker.process.pid}`);
//   });
//   cluster.on("exit", (worker) => {
//     console.log(
//       `Worker Id ${worker.id} and PID ${worker.process.pid} is offline`
//     );
//     // fork new woker for it
//     cluster.fork();
//   });
// } else {
//   app.listen(process.env.PORT || 4000, () => {
//     console.log(`server on http://localhost:${process.env.PORT || 4000}`);
//   });
// }

app.listen(process.env.PORT || 4000, () => {
  console.log(`server on http://localhost:${process.env.PORT || 4000}`);
});
