import { Router } from "express";
import { getPermissions } from "../../handlers/permissions";

const router = Router();

router.get("/", getPermissions);

export default router;
