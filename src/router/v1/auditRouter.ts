import { Router } from "express";
import { requestErrorHandler } from "../../modules/validate";
import { createAuditHistory } from "../../handlers/audit";
import { reqValidationCreateAuditHistory } from "../../handlers/validate";

const router = Router();

// TODO: admin get all audit history from admin's company
// router.get("/", () => {});

// removed, no need to get all audit history by login user
// router.get("/owned", getOwnedAuditHistories);

// add a new audit history record by login user (req.user)
router.post(
  "/",
  reqValidationCreateAuditHistory,
  requestErrorHandler,
  createAuditHistory
);
// will not open put/delete to client, since no need for modify audit history
// router.put(
//   "/:id",
//   body("title").isString(),
//   body("active").isIn(["EDIT", "DELETE", "ADD"]),
//   requestErrorHandler,
//   () => {}
// );
// router.delete("/:id", () => {});

export default router;
