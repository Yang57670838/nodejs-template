import { Router } from "express";
import { requestErrorHandler } from "../../modules/validate";
import {
  createNewUser,
  updateUser,
  deleteUser,
  getColleagues,
  getOneUser,
  getAvailableIframes,
} from "../../handlers/user";
import { getAuditHistories } from "../../handlers/audit";
import {
  reqValidationCreateUuser,
  reqValidationUpdateUser,
} from "../../handlers/validate";

const router = Router();

// get available iframe projects from login user
router.get("/availableIframes", getAvailableIframes);

// get all users belong to login user's company
router.get("/", getColleagues);
// geta user detail based on user id, and must be in login user's company.. (prevent if someone can guess random user id)
router.get("/:id", getOneUser);
// create a new user by system admin, but must be in login admin's company  (prevent if someone can guess random company id)
router.post("/", reqValidationCreateUuser, requestErrorHandler, createNewUser);
// update a user, but must be in login admin's company  (prevent if someone can guess random user id)
router.put("/:id", reqValidationUpdateUser, requestErrorHandler, updateUser);
// delete a user, and must be in login user's company.. (prevent if someone can guess random user id)
router.delete("/:id", deleteUser);

// admin get all audit history from one particular user's id
router.get("/:id/audits", getAuditHistories);

export default router;
