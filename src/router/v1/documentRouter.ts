import { Router } from "express";
import { downloadtermcondition } from "../../handlers/user";

const router = Router();

router.get("/term-condition", downloadtermcondition);

export default router;
