import { Router } from "express";
import { requestErrorHandler } from "../../modules/validate";
import { searchClients, getClients } from "../../handlers/client";

const router = Router();

router.post("/search", requestErrorHandler, searchClients);
router.get("/", requestErrorHandler, getClients);

export default router;
