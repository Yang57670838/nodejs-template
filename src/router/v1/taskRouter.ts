import { Router } from "express";
import { requestErrorHandler } from "../../modules/validate";
import { addOneTaskHandler } from "../../handlers/tasks";

const router = Router();

router.post("/", addOneTaskHandler);

export default router;
