import { Router } from "express";
import { onetimePayment } from "../../handlers/payment";

const router = Router();

// one time pay
router.post("/", onetimePayment);

export default router;
