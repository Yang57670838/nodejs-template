import { Router } from "express";
import paymentRouter from "./v1/paymentRouter";
import userRouter from "./v1/userRouter";
import taskRouter from "./v1/taskRouter";
import auditRouter from "./v1/auditRouter";
import permissionRouter from "./v1/permissionRouter";
import clientRouter from "./v1/clientRouter";
import documentRouter from "./v1/documentRouter";

const router = Router();

router.use("/users", userRouter);
router.use("/tasks", taskRouter);
router.use("/audits", auditRouter);
router.use("/permissions", permissionRouter);
router.use("/payments", paymentRouter);
router.use("/clients", clientRouter);
router.use("/documents", documentRouter);

export default router;
// version handler
// app.use("/api/documents", protect, documentVersionRegister);
