import { Router } from "express";
import { Request, Response } from "express";

const router = Router();

router.get("/term-condition", (req: Request, res: Response) => {
  res.status(200);
  res.json({ message: "you are reaching API version 2 example" });
});

export default router;
