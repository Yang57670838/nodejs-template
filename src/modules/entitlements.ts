import { FeatureEntitlement } from "../types/permissions";

export const formatEntitlementsToFeatureToggles = (
  entitlementList: Array<string>,
  allows: Array<string>
): Array<FeatureEntitlement> => {
  return entitlementList.map((e) => {
    return {
      name: e,
      active: allows.includes(e),
    };
  });
};
