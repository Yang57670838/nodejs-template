import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { User } from "@prisma/client";
import { Request, Response, NextFunction } from "express";

export const comparePasswords = (
  password: string,
  hash: string
): Promise<boolean> => {
  return bcrypt.compare(password, hash);
};

export const hashPassword = (password: string): Promise<string> => {
  return bcrypt.hash(password, 12); // use 12 to make hash complex
};

export const createJWT = (user: User): string => {
  const token = jwt.sign(
    {
      id: user.id,
      email: user.email,
      belongsToId: user.belongsToId,
      username: user.username,
      role: user.role,
      name: user.name,
    },
    process.env.JWT_SECRET as string,
    {
      expiresIn: "1d",
    }
  );
  return token;
};

export const protect = (req: Request, res: Response, next: NextFunction) => {
  const bearer = req.headers.authorization;
  if (!bearer) {
    res.status(401);
    res.json({ message: "not authorized" });
    return;
  }
  // seems sending from axios -> webpack dev server will have no Bearer, send from rtk query will have Bearer
  let token;
  if (bearer.includes("Bearer")) {
    //"Bearer asdksdadaedaqewqewqq"
    token = bearer.split(" ")[1];
  } else {
    token = bearer;
  }
  if (!token) {
    res.status(401);
    res.json({ message: "not valid token" });
    return;
  }
  try {
    const user = jwt.verify(token, process.env.JWT_SECRET as string);
    req.user = user; // each req can access login user easily
    next();
  } catch (e) {
    console.error(e);
    res.status(401);
    res.json({ message: "not valid token" });
    return;
  }
};
