import express, { Application } from "express";
import helmet from "helmet";
import morgan from "morgan";
import cors from "cors";
import { protect } from "./modules/auth";
import { signin, changepassword } from "./handlers/user";
import { rootHandler, ErrorHandler } from "./handlers/root";
import rootController from "./router/rootController";

const app: Application = express();
const isProd = process.env.NODE_ENV === "production";

app.use(morgan(isProd ? "common" : "dev"));
app.use(helmet());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

app.get("/", rootHandler);
app.use("/api", protect, rootController);
app.post("/signin", signin);
app.post("/changepassword", changepassword);
// dont need to user register himself, will only allow log in admin register new user for his company
// app.post('/register', createNewUser)

// global error handler
app.use(ErrorHandler);

export default app;
