import { Request, Response, NextFunction } from "express";
import prisma from "../db";
import { ExpressError } from "../types/common";

// geta some clients based on search payload, and must be in login user's company.. (prevent if someone can guess random user payload)
export const searchClients = async (req: Request, res: Response) => {
  // TODO: !Important: when return, dont return sensitive data, like password!!!
  const result = await prisma.client.findMany({
    where: {
      firstname: {
        contains: req.body.name || "",
      },
      email: {
        contains: req.body.email || "",
      },
    },
  });
  res.json({ data: result });
};

export const getClients = async (req: Request, res: Response) => {
  // TODO: !Important: when return, dont return sensitive data, like password!!!
  const result = await prisma.client.findMany();
  res.json({ data: result });
};
