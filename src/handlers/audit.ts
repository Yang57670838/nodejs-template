import { Request, Response, NextFunction } from "express";
import { UserEntity } from "../types/user";
import prisma from "../db";

// TODO: use MONGODB connection for audits, refactor this file.., since its no complex relation, and big data..

// admin get all audit history from one user's id, make sure
// TODO: make sure if this user is same company as login user (check token)
// TODO: make sure login user is admin
export const getAuditHistories = async (req: Request, res: Response) => {
  const user = await prisma.user.findUnique({
    where: {
      id: req.params.id,
    },
    include: {
      auditHistory: true,
    },
  });
  res.json({ data: (user as UserEntity).auditHistory });
};

// get all audit history by login user
export const getOwnedAuditHistories = async (req: Request, res: Response) => {
  const user = await prisma.user.findUnique({
    where: {
      id: req.user.id,
    },
    include: {
      auditHistory: true,
    },
  });
  res.json({ data: (user as UserEntity).auditHistory });
};

// add a new audit history record by login user (req.user)
export const createAuditHistory = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const history = await prisma.auditHistory.create({
      data: {
        title: req.body.title,
        active: req.body.active,
        userId: req.user.id,
      },
    });
    res.json({ data: history });
  } catch (e) {
    next(e);
  }
};
