import { Request, Response, NextFunction } from "express";
import { ExpressError } from "../types/common";

export const rootHandler = (req: Request, res: Response) => {
  console.log(`Request came to pm2 process ${process.argv[0]}`);
  res.status(200);
  res.json({ message: "nodejs template app" });
};

export const ErrorHandler = (
  err: ExpressError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (process.env.NODE_ENV !== "production") {
    console.error(err.stack);
  }
  if (err.type === "auth") {
    res.status(401).json({ message: "unauthorized" });
  } else if (err.type === "input") {
    res.status(400).json({ message: "invalid input" });
  } else {
    res.status(500).send("Something broke!");
  }
};
