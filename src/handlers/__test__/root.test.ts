import { rootHandler, ErrorHandler } from "../root";
import {
  mockExpressInputError,
  mockExpressAuthError,
  mockExpressUndefinedError,
} from "../../mocks/error";

describe("handlers for root server.ts", () => {
  test("rootHandler responds to /", () => {
    const req: any = {};
    const res: any = {
      statusResult: "",
      jsonResult: "",
      status: function (input: any) {
        this.statusResult = input;
      },
      json: function (input: any) {
        this.jsonResult = input;
      },
    };
    rootHandler(req, res);

    expect(res.statusResult).toEqual(200);
    expect(res.jsonResult.message).toEqual("nodejs template app");
  });

  test("ErrorHandler responds to gloabl thrown errors", () => {
    const req: any = {};
    const mockResponse: any = {
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
      send: jest.fn(),
    };
    const nextFunction = jest.fn();

    ErrorHandler(mockExpressInputError, req, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(400);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "invalid input",
    });
    expect(nextFunction).not.toHaveBeenCalled();

    ErrorHandler(mockExpressAuthError, req, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(400);
    expect(mockResponse.json).toHaveBeenCalledWith({
      message: "invalid input",
    });
    expect(nextFunction).not.toHaveBeenCalled();

    ErrorHandler(mockExpressUndefinedError, req, mockResponse, nextFunction);
    expect(mockResponse.status).toHaveBeenCalledWith(400);
    expect(mockResponse.send).toHaveBeenCalledWith("Something broke!");
    expect(nextFunction).not.toHaveBeenCalled();
  });
});
