import { Request, Response, NextFunction } from "express";
import * as path from "path";
import prisma from "../db";
import { hashPassword, createJWT, comparePasswords } from "../modules/auth";
import { ExpressError } from "../types/common";
import { moockIframeProjects } from "../mocks/availableIframeProjectsMock";

// create a new user by system admin, but must be in login admin's company  (prevent if someone can guess random company id)
export const createNewUser = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    const user = await prisma.user.create({
      data: {
        username: req.body.username,
        password: await hashPassword(req.body.password),
        email: req.body.email,
        belongsToId: req.user.belongsToId,
        role: req.body.role,
        name: req.body.name,
      },
    });
    // const user = {
    //     username: "tom",
    //     email: "awa@gmail.com",
    //     belongsToId: "12345",
    //     role: "ADMIN",
    //     name: "David Jones",
    //     id: "12345123245"
    // }
    const token = createJWT(user);
    res.json({ token });
  } catch (e) {
    (e as ExpressError).type = "input";
    next(e);
  }
};

// update a user, but must be in login admin's company  (prevent if someone can guess random user id)
export const updateUser = async (req: Request, res: Response) => {
  const updated = await prisma.user.update({
    where: {
      id_belongsToId: {
        id: req.params.id,
        belongsToId: req.user.belongsToId,
      },
    },
    data: {
      name: req.body.name,
      role: req.body.role,
      username: req.body.username,
      password: req.body.password,
      email: req.body.email,
    },
  });
  res.json({ data: updated });
};

// delete a user, and must be in login user's company.. (prevent if someone can guess random user id)
export const deleteUser = async (req: Request, res: Response) => {
  const deleted = await prisma.user.delete({
    where: {
      id_belongsToId: {
        id: req.params.id,
        belongsToId: req.user.belongsToId,
      },
    },
  });
  res.json({ data: deleted });
};

export const signin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  // TODO: fix wrong credential hang there,, should res error
  console.log("someone is login", req.body);
  try {
    const user = await prisma.user.findUnique({
      where: {
        email: req.body.email,
      },
    });
    if (user) {
      const isValid = await comparePasswords(req.body.password, user.password);
      if (!isValid) {
        res.status(401);
        res.json({ message: "not valid token" });
        return;
      }
      const token = createJWT(user);
      res.json({ token });
    }
  } catch (e) {
    next(e);
  }
};

// get all users belong to login user's company
export const getColleagues = async (req: Request, res: Response) => {
  const colleagues = await prisma.user.findMany({
    where: {
      belongsToId: req.user.belongsToId,
    },
  });
  res.json({ data: colleagues });
};

// geta user detail based on user id, and must be in login user's company.. (prevent if someone can guess random user id)
export const getOneUser = async (req: Request, res: Response) => {
  // TODO: !Important: when return, dont return sensitive data, like password!!!
  const colleagues = await prisma.user.findUnique({
    where: {
      id_belongsToId: {
        id: req.params.id,
        belongsToId: req.user.belongsToId,
      },
    },
  });
  res.json({ data: colleagues });
};

export const changepassword = async (req: Request, res: Response) => {
  // TODO: validate one time OTP against existing email account now, change password, then let /signin to handle
  console.log("req.body", req.body);
  res.redirect(307, "/signin");
};

export const downloadtermcondition = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.set("Cache-Control", "no-store");
  res.set("Content-Type", "application/pdf");
  res.download(
    path.join(__dirname, "../assets/files/term_condition_sample.pdf"),
    "TermCondition.pdf",
    (err) => {
      next(err);
    }
  );
};

// TODO: add db for it, refactor this
export const getAvailableIframes = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  res.json(moockIframeProjects);
};
