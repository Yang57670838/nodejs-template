import { body } from "express-validator";

export const reqValidationCreateUuser = [
  body("username").isString(),
  body("password")
    .trim()
    .isString()
    .isLength({ min: 4, max: 20 })
    .withMessage("Password must be min 4 and max 20 chars"),
  body("email").isEmail().withMessage("Email must be valid"),
  body("name").optional().isString(),
  body("role").isIn(["NORMAL", "ADMIN"]),
];

export const reqValidationUpdateUser = [
  body("username").optional().isString(),
  body("password").optional().isString(),
  body("email").optional().isEmail(),
  body("name").optional().isString(),
  body("role").optional().isIn(["NORMAL", "ADMIN"]),
];

export const reqValidationCreateAuditHistory = [
  body("title").isString(),
  body("active").isIn(["EDIT", "DELETE", "ADD"]),
];
