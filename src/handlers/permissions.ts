import { Request, Response, NextFunction } from "express";
import { entitlements } from "../constants/entitlements";
import { formatEntitlementsToFeatureToggles } from "../modules/entitlements";
import prisma from "../db";

export const getPermissions = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    let result;
    const permissions = await prisma.permissions.findUnique({
      where: {
        belongsToUser: req.user.id,
      },
    });
    if (permissions && permissions.allow.length > 0) {
      result = formatEntitlementsToFeatureToggles(
        Object.keys(entitlements),
        permissions?.allow
      );
    } else {
      result = formatEntitlementsToFeatureToggles(
        Object.keys(entitlements),
        []
      );
    }
    res.json({ data: result });
  } catch (e) {
    next(e);
  }
};
