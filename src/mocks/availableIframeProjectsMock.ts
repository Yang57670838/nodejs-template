import { IframeProject } from "../types/iframe";

export const moockIframeProjects: Array<IframeProject> = [
  {
    appId: "1111",
    appName: "Register Form",
    link: "http://localhost:3000",
  },
  {
    appId: "2222",
    appName: "Client Survey",
    link: "https://www.google.com",
  },
];
