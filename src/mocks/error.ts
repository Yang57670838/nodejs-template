import { ExpressError } from "../types/common";

export const mockExpressInputError: ExpressError = {
  name: "error",
  message: "string",
  type: "input",
};

export const mockExpressAuthError: ExpressError = {
  name: "error",
  message: "string",
  type: "auth",
};

export const mockExpressUndefinedError: ExpressError = {
  name: "error",
  message: "string",
  type: "",
};
