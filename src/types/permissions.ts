export interface FeatureEntitlement {
  name: string;
  active: boolean;
}
