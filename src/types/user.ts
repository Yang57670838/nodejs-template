import { User } from "@prisma/client";

export interface UserEntity extends User {
  auditHistory: Array<any>;
}
