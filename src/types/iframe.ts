export interface IframeProject {
  appId: string;
  appName: string;
  link: string;
}
