import app from "../server";
import supertest from "supertest";

describe("GET /", () => {
  it("should handle root GET correctly", async () => {
    const res = await supertest(app).get("/");

    expect(res.body.message).toBe("nodejs template app");
  });
});

describe("POST /signin", () => {
  it("should handle successful signin correctly", async () => {
    const res = await supertest(app).post("/signin").send({
      email: "jerry@gmail.com",
      password: "Jerry",
    });
    expect(typeof res.body.token).toEqual("string");
    expect(res.status).toEqual(200);
    expect(res.body.token.split(".").length).toEqual(3);
    console.log("current token for signin test", res.body.token);
  });
});
