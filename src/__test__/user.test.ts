import app from "../server";
import supertest from "supertest";
import { createJWT } from "../modules/auth";
import prisma from "../db";

describe("user endpoints", () => {
  let token: string;
  beforeAll(async () => {
    // login
    const userJerry = await prisma.user.findUnique({
      where: {
        email: "jerry@gmail.com",
      },
    });
    if (userJerry) {
      console.log("login with user Jerry");
      token = createJWT(userJerry);
    }
  });

  describe("GET /api/users", () => {
    it("should GET all colleagues details by login user correctly", async () => {
      const res = await supertest(app)
        .get("/api/users")
        .set("authorization", token);
      expect(res.status).toBe(200);
      expect(res.body.data.length).toBe(2);
    });
  });

  describe("POST /api/users", () => {
    it("should create a new user belongs to login user's company, and save hashed password", async () => {
      const res = await supertest(app)
        .post("/api/users")
        .send({
          username: "David",
          password: "1234567",
          email: "david@gmail.com",
          role: "NORMAL",
        })
        .set("authorization", token);
      expect(res.status).toBe(200);
      expect(res.body.token.split(".").length).toBe(3);
      const newUser = await prisma.user.findUnique({
        where: {
          email: "david@gmail.com",
        },
      });
      expect(newUser).not.toBeNull();
      if (newUser) {
        expect(newUser.username).toEqual("David");
        // hash password in db test
        expect(newUser.password).not.toEqual("1234567");
      }
    });
  });
});
