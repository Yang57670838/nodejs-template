// list of all permissions we want to limit to employees
export enum entitlements {
  "view.products" = "view.products",
  "add.products" = "add.products",
  "view.task" = "view.task",
  "add.task" = "add.task",
}
