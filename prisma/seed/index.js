const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const users = require("./user");
const companies = require("./company");
const permissions = require("./permissions");
const clients = require("./client");

async function runSeeders() {
  // companies
  await Promise.all(
    companies.map(async (c) =>
      prisma.company.upsert({
        where: { id: c.id },
        update: {},
        create: c,
      })
    )
  );

  // Users
  await Promise.all(
    users.map(async (user) =>
      prisma.user.create({
        data: {
          id: user.id,
          username: user.username,
          password: user.password,
          email: user.email,
          belongsToId: user.belongsToId,
          role: user.role,
        },
      })
    )
  );

  // permissions
  await Promise.all(
    permissions.map(async (p) =>
      prisma.permissions.create({
        data: {
          belongsToUser: p.belongsToUser,
          allow: p.allow,
        },
      })
    )
  );

  // clients
  await Promise.all(
    clients.map(async (client) =>
      prisma.client.create({
        data: {
          firstname: client.firstName,
          lastname: client.lastName,
          email: client.email,
          belongsToId: client.belongsToId,
          mobile: client.mobile,
        },
      })
    )
  );
}

runSeeders()
  .catch((e) => {
    console.error(`There was an error while seeding: ${e}`);
    process.exit(1);
  })
  .finally(async () => {
    console.log("Successfully seeded database. Closing connection.");
    await prisma.$disconnect();
  });
