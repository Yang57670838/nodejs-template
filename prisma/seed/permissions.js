module.exports = [
  // user 1 permissions
  {
    belongsToUser: "454bea80-f59b-48ae-90b8-a5284968e36f",
    // TODO: add endpoint guard for this action with this permission limit as well
    allow: ["view.products", "view.task"], // for particular actions, limit user permissions,
  },
  // user 2 permissions
  {
    belongsToUser: "79ef75aa-377e-457c-b0a9-20d053fe9699",
    allow: ["view.products", "view.task", "add.products", "add.task"], // mock this admin user have full permissions
  },
];
