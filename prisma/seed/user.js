module.exports = [
  // user 1
  {
    id: "454bea80-f59b-48ae-90b8-a5284968e36f",
    password: "$2b$12$J.2vYWoNpI2cp2UzeNoUwOytJKkeyWi0igqWnhQQP5UEGMzs9pc..", // password is Tom
    username: "Tom",
    email: "tom@gmail.com",
    belongsToId: "9197dea8-1d94-11ec-9621-0242ac130002",
    role: "NORMAL",
  },
  // user 2
  {
    id: "79ef75aa-377e-457c-b0a9-20d053fe9699",
    password: "$2b$12$62wvaB8X7BiyVlVyHorFkuzWDUAUtECWF8NjeRyeCI2fmWmzVTA9e", // password is Jerry
    username: "Jerry",
    email: "jerry@gmail.com",
    belongsToId: "9197dea8-1d94-11ec-9621-0242ac130002",
    role: "ADMIN",
  },
];
