module.exports = [
  // client 1
  {
    id: "1",
    firstName: "David",
    lastName: "Chen",
    email: "1@gmail.com",
    mobile: "0522326782",
    belongsToId: "9197dea8-1d94-11ec-9621-0242ac130002",
  },
  // client 2
  {
    id: "2",
    firstName: "Jones",
    lastName: "Smite",
    email: "2@gmail.com",
    mobile: "0412323242",
    belongsToId: "9197dea8-1d94-11ec-9621-0242ac130002",
  },
];
