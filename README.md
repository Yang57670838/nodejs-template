## local run orchestration with dockerized express app and postgres
### run
yarn run compose:init<br />
now can see database interface by: yarn run show:db:local

### stop
docker-compose down -v

## use pm2 in prod server instead of dockerized
### pre-required commands
npm install -g pm2 <br />
yarn install --frozen-lockfile <br />
yarn build <br />
npx prisma generate

### prepare env file
.env.production

### if have postgres connection, and first time run it, be careful since its production
yarn run dangerous:migrate:postgres:prod

### start app with pm2 in prod
pm2 start ecosystem.config.js --env production

### stop the app
pm2 stop all

### delete the app process
pm2 delete all

### notes
make sure target ec2 have 1GB memory, since max_memory_restart of pm2 is set to 900M

## testing
docker-compose down -v <br />
yarn run test:coverage <br />
docker-compose down -v <br />